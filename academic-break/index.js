// -----------T A S K 1 ------------

	function shipItem(item, weight, location) {
	    let shippingFee = 0;

	    // Set condition for shipping fee based on weight
	    if (weight == 3) {
	        shippingFee = 150;
	    } else if (weight >= 5) {
	        shippingFee = 280;
	    } else if (weight >= 8) {
	        shippingFee = 340;
	    } else if (weight >= 10) {
	        shippingFee = 410;
	    } else if (weight > 10) {
	        shippingFee = 560;
	    } else {
	        return "Weight invalid";
	    }

	    switch (location) {
	        case "local":
	            shippingFee += 0;
	            break;
	        case "international":
	            shippingFee += 250;
	            break;
	        default:
	            break;
	    }

	    return `The total shipping fee for an ${item} that will ship ${location} is ${shippingFee}.`;
	}

	 // console.log(shipItem("GPRO", 3, "international"));
	 // console.log(shipItem("GMMK", 5, "international"));
	 // console.log(shipItem("BlacksharkV2", 3, "Local"));

     // ------------ T A S K 2 ---------------


	 function Product(name, price, quantity) {
	   this.name = name;
	   this.price = price;
	   this.quantity = quantity;
	 }
	 
	 function Customer(name, cart, addToCart, removeToCart){
        this.name = name;
        this.cart = [];
        this.addToCart=function(product){
            this.cart.push(product);
            return `${product.name} added to cart,`
        };
        this.removeToCart=function(product){
            let check = this.cart.includes(product);
            if(check){
                let productIndex = this.cart.indexOf(product);
                let removedItem = this.cart.splice(productIndex,1);
                return `${product.name} is removed from the cart.`;
           }  else {
                return `${product.name} is not in the cart.`;
            }
        };
}



	 // New Customer
	 let chazz = new Customer("Chazz");

	 // New Products
	 let gmmk = new Product("GMMK", 5500, 1);
	 let gpro = new Product("GPRO", 8900, 1);
	 let blacksharkv2 = new Product("BlackSharkV2", 4500, 1);

	 // console.log(chazz.addToCart(GPRO));
	 // console.log(chazz.addToCart(GMMK));
	 // console.log(chazz.addToCart(BlackSharkV2));
	 // console.log(chazz.removeToCart(GPRO));