// console.log("!!!!!!!!!!!!!!!!!!!!!")

// function printINput(){
// 	let nickname = prompt("Enter your nickname: ")
// 	return "Hi " + nickname;
// }

// printINput();

/*
You can directly pass data into the function. The function can then call/use that data which is referred as "name" within the function.

"name" is called a parameter.

A "parameter" acts as a named variable/container that exists only inside of a function
It is used to store information that is provided to a function when it is called/invoked.

the information/data provided directly into the function, is called an argument. Values passed when invoking a function are called arguments. These arguments are then stored as the parameters within the function.

// parameters with the prompt don't need hardcoded arguments since the values will be provided via prompt

*/

function printName(name){
	return "My name is " + name;
}
/*
	the Information.data provided directly into the function, is called an argument. Values passed when invoking a function are called arguments. These arguments are then stored as the parameters within the fucntion
*/

console.log(printName("Pat"))

let anotherName = "Mitsuha"
console.log(printName(anotherName));

function printInput(nickname){

	nickname = prompt("Enter your nickname: ")
 	return "Hi, " + nickname;
}

// console.log(printInput());

function checkDivisibilityBy8(num){
	let remainder = num % 8;
	console.log("The remainder of " + num + " divisible by 8 :");

	let isDivisibleBy8 = remainder ===8;
	console.log("Is " + num + "divisible by 8?");
	console.log(isDivisibleBy8)
}
checkDivisibilityBy8(64);
checkDivisibilityBy8(28);

/*
	Function parameters can also accept other functions as arguments
	Some complex functions use other functions as arguments to perform more complicated results.
	This will be further seen when we discuss array methods.

*/

function argumentFunction(){
	console.log("This was passed as an argument before the message was printed.");
};

function invokeFunction(argumentFunction){
	argumentFunction();
}

invokeFunction(argumentFunction);

console.log(argumentFunction);

//Using Multiple Parameters
/*
	Multiple "arguments" will correspond to the number of "parameters" declared in a function in succeeding order.
*/

function createFullname(firstName, middleName, lastName){
	return firstName + " " + middleName + " " + lastName;
}

console.log(createFullname("Juan", "Dela", "Cruz"));