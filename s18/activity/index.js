let message = "No message.";
  console.log(message);

  function determineTyphoonIntensity(windSpeed){

    if (windSpeed < 30) {
      return "Not a typhoon yet.";

    } else if (windSpeed <= 61) {
      return "Tropical depression detected.";

    } else if (windSpeed >= 62 && windSpeed <= 88) {
      return "Tropical storm detected.";

    } else if (windSpeed >= 89 || windSpeed <= 117) {
      return "Severe tropical storm detected."

    } else {
      return "Typhoon detected.";
    }

  };

  message = determineTyphoonIntensity(62);
  console.log(message);
