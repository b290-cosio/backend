// console.log("Hello World");

//Strictly Follow the property names and spelling given in the google slide instructions.
//Note: Do not change any variable and function names. 
//All variables and functions to be checked are listed in the exports.

// Create an object called trainer using object literals

// Initialize/add the given object properties and methods

// Properties

// Methods

// Check if all properties and methods were properly added


// Access object properties using dot notation

// Access object properties using square bracket notation

// Access the trainer "talk" method


// Create a constructor function called Pokemon for creating a pokemon


// Create/instantiate a new pokemon


// Create/instantiate a new pokemon


// Create/instantiate a new pokemon


// Invoke the tackle method and target a different object


// Invoke the tackle method and target a different object

let trainer = {
  name: "Ash Ketchum",
  age: 10,
  Pokemon: ["Pikachu", "Charizard", "Squirtle", "Bulbasaur"],
  friends: {
    hoenn: ["May", "Max"],
    kanto: ["Brock", "Misty"]
  },
  
  talk: function() {
    return "Pikachu, I choose you!";
  }
};

function Pokemon(name, level) {
  this.name = name;
  this.level = level;
  this.health = 2 * level;
  this.attack = level;

  this.tackle = function(target) {
    target.health -= this.attack;

    if (target.health <= 0) {
      console.log(`${this.name} used Tackle on ${target.name}!`);
      console.log(`${target.name}'s health is now ${target.health}.`);
      this.faint(target);
    } else {
      console.log(`${this.name} used Tackle on ${target.name}!`);
      console.log(`${target.name}'s health is now ${target.health}.`);
    }
  };

  this.faint = function() {
    console.log(`${this.name} fainted!`);
  };
}

let pikachu = new Pokemon("Pikachu", 12);
let geodude = new Pokemon("Geodude", 8);
let mewtwo = new Pokemon("Mewtwo", 100);


//Do not modify
//For exporting to test.js
//Note: Do not change any variable and function names. All variables and functions to be checked are listed in the exports.
try{
    module.exports = {

        trainer: typeof trainer !== 'undefined' ? trainer : null,
        Pokemon: typeof Pokemon !== 'undefined' ? Pokemon : null

    }
} catch(err){

}